import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Job } from './job';
import { JOBS } from './mock-jobs';


@Injectable()
export class JobService {
  

 
  getOneJob(): Promise<Job> {
    return Promise.resolve(JOBS);
  }

  // See the "Take it slow" appendix
  getOneJobSlowly(): Promise<Job> {
    return new Promise(resolve => {
      // Simulate server latency with 2 second delay
      setTimeout(() => resolve(this.getOneJob()), 2000);
    });
  }
 


private amazonAPI = 'https:\/\/9bll91e0xk.execute-api.us-east-1.amazonaws.com/dev?postId='; 
 
constructor(private http: Http) { }
 
 /*
getJob(): Promise<Job[]> {
  return this.http.get(this.amazonAPI)
             .toPromise()
             .then(response => response.json().data as Job[])
             .catch(this.handleError);
}*/

 getJob(uid: string): Promise<Job> {
   console.log('getJob uid',{uid});
    const url = `${this.amazonAPI}${uid}`;
    return this.http.get(url)
      .toPromise()
     .then(response => response.json()[0] as Job)
     /*.then(response => {
        console.log('From Promise:', response.json()[0]);
      })*/
     .catch(this.handleError);
  }
 
private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}



}




