import 'rxjs/add/operator/switchMap';
import { Component,Input,OnInit,DoCheck,Renderer2 } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import { Voice } from './voice';
import { Job } from './job';
import { VoiceService } from './voice.service';
import { JobService } from './job.service';

@Component({
  selector: 'voice-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})

export class JobComponent implements OnInit, DoCheck {
  title = 'Best Speech Bot';
  playing = false;
  downloadable = true;
  playing_track = null;
  loading=false;
  job_text = "";

  voices: Voice[];
  job: Job;
  //selectedVoice: Voice;

  constructor(
  	private voiceService: VoiceService,
  	private jobService: JobService,
  	private route: ActivatedRoute,
    private location: Location,
    private renderer: Renderer2
  	) { }

	getVoices(): void {
		this.voices = this.voiceService.getVoices();

		//this.voiceService.getVoices().then(voices => this.voices = voices);
	}

	getJob(): void {

		//this.job = this.jobService.getJob();
		//this.jobService.getOneJob().then(job => this.job = job);
		//console.log(this.route);

	}

	newPlay(): void{
		this.playing_track = new Audio(this.job.url);
	}

	play(): void{


			this.playing = true;

			//check if user asking to play changed text
			if(this.job_text == this.job.text){

					//if play hasn't started, start a newone
					if(!this.playing_track){
						this.newPlay();
					}

					//start playing
					this.playing_track.play();

					//listenr for when play finished.
					this.renderer.listen(this.playing_track, 'ended', (evt) => {
						this.stopPlay();
					})

			}else{

				this.updatedPlay();				
			}

	}

	stopPlay(): void{

			this.playing = false;
			this.playing_track.pause();
	}

	updatedPlay(): void{

		this.loading = true;


	}

	download(): void{

	}

	//on initialization
	ngOnInit(): void {
		this.getVoices();
		//this.getJob();

		console.log(this.route.paramMap);
		
		this.route.paramMap
		  .switchMap((params: ParamMap) => this.jobService.getJob(params.get('uid')))
		  .subscribe(job => {this.job = job; this.job_text = job.text; } ,
		  	err => console.error(err), 
		  	() => console.log('test',this.job.text)
		  	);

		  //this.job_text = this.job.text;
		 
	}

	ngDoCheck() {


    
    }


}
