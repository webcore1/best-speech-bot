export class Job {
  uid: string;
  text: string;
  url: string;
  voice: string;
  status: string;
}