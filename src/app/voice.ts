export class Voice {
  id: string;
  name: string;
  code: string;
}