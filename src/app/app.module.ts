import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { JobComponent } from './job.component';
import { NgStyle } from '@angular/common';
import { VoiceService } from './voice.service';
import { JobService } from './job.service';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
    declarations: [
    AppComponent,
    JobComponent
  ],
  providers: [VoiceService,JobService],
  bootstrap: [AppComponent]
})
export class AppModule { }
