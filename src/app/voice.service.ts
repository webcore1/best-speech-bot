import { Injectable } from '@angular/core';
import { Voice } from './voice';
import { VOICES } from './mock-voices';


//without promise
@Injectable()
export class VoiceService {
  getVoices(): Voice[] {
    return VOICES;
  }
}

/*

//with promise

@Injectable()
export class VoiceService {
  getVoices(): Promise<Voice[]> {
    return Promise.resolve(VOICES);
  }

  // See the "Take it slow" appendix
  getVoicesSlowly(): Promise<Voice[]> {
    return new Promise(resolve => {
      // Simulate server latency with 2 second delay
      setTimeout(() => resolve(this.getVoices()), 2000);
    });
  }
}
*/